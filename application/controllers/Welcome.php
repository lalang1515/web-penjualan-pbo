<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->home();
	}

	public function home()
	{
		$this->load->view('home');
	}

	public function login()
	{
		$this->load->database();
		
		$user = $this->db
		->select('*')
		->from('tbl_user')
		->get()
		->result_array();

		echo json_encode($user); die;
	}

	public function registery()
	{
		$this->load->view('register');
	}

	public function register()
	{
		$data = $this->input->post();

		$this->load->database();

		if ($data['username'] && $data['email'] && $data['password']) {
			$ins = array(
				'username' => $data['username'],
				'email' => $data['email'],
				'password' => $data['password']
			);	
		
			$login_status = $this->db->insert('tbl_user', $ins);
		} else {
			$login_status = false;
		}

		echo json_encode(array(
			'status' => $login_status
		)); die;
	}

	public function beranda($path = 'index')
	{
		if ($path == 'index') {
			$path = 'produk';
		}

		$data = array(
			'path' => $path
		);

		$this->load->database();

		if ($path == 'produk') {
			$produk = $this->db
				->select('*')
				->from('tbl_produk')
				->get()
				->result_array();

			$data['produk'] = $produk;
		}

		if ($path == 'penjualan') {
			$penjualan = $this->db
				->select('*')
				->from('tbl_penjualan')
				->get()
				->result_array();

			$data['penjualan'] = $penjualan;
		}

		$this->load->view('beranda', $data);
	}

	public function get_produk()
	{
		$search = $this->input->post();

		$this->load->database();

		$produk = $this->db
				->select('*')
				->from('tbl_produk')
				->like('nama', $search['textToSearch'])
				->or_like('nama', $search['textToSearch'])
				->get()
				->result_array();

		echo json_encode($produk); die;
	}

	public function tambah_produk($data)
	{
		$this->load->database();

		if ($data['nama_produk'] && $data['harga_satuan'] && $data['img']) {
			$ins = array(
				'nama' => $data['nama_produk'],
				'harga_satuan' => $data['harga_satuan'],
				'img' => $data['img']
			);	
		
			$tambah_produk_status = $this->db->insert('tbl_produk', $ins);
		} else {
			$tambah_produk_status = false;
		}

		if ($tambah_produk_status) {
			redirect('http://localhost/web-penjualan-pbo/index.php/welcome/beranda/produk', 'refresh');
		}
	}

	public function hapus_produk()
	{
		$data = $this->input->post();

		$this->load->database();

		if ($data['id']) {
			unlink('application/uploads/produk/'.$data['img']);

			$hapus_produk_status = $this->db->delete('tbl_produk', array('id' => $data['id']));
		} else {
			$hapus_produk_status = false;
		}

		echo json_encode(array(
			'status' => $hapus_produk_status
		)); die;
	}

	public function edit_produk($produk = '')
	{
		$data = $produk;

		$this->load->database();

		if (isset($data['id']) && isset($data['nama_produk']) && isset($data['harga_satuan']) && isset($data['img'])) {
			$ins = array();

			if (isset($data['nama_produk'])) {
				$ins['nama'] = $data['nama_produk'];
			}
			if (isset($data['harga_satuan'])) {
				$ins['harga_satuan'] = $data['harga_satuan'];
			}
			if ($data['img'] != 0) {
				$ins['img'] = $data['img'];
			}

			$this->db->where('id', $data['id']);
			$edit_produk_status = $this->db->update('tbl_produk', $ins);
		} else {
			$edit_produk_status = false;
		}

		if ($edit_produk_status) {
			redirect('http://localhost/web-penjualan-pbo/index.php/welcome/beranda/produk', 'refresh');
		}
	}

	public function do_upload()
	{
		$produk = $this->input->post();

		if ($this->input->method() === 'post') {
			$config['upload_path']          = './application/uploads/produk/';
			$config['allowed_types']        = 'jpg|jpeg|png|jfif';
			$config['encrypt_name']         = true;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('produk_img')) {
				echo var_dump($this->upload->display_errors()); die;
			} else {
				$produk['img'] = $this->upload->data("file_name");

				$this->tambah_produk($produk);
			}
		}
	}

	public function edit_upload($id = '')
	{
		$produk = $this->input->post();

		$produk['id'] = $id;

		if ($this->input->method() === 'post') {
			$config['upload_path']          = './application/uploads/produk/';
			$config['allowed_types']        = 'jpg|jpeg|png|jfif';
			$config['encrypt_name']         = true;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('produk_img')) {
				$this->edit_produk($produk);
			} else {
				unlink('application/uploads/produk/'.$produk['img']);
				
				$produk['img'] = $this->upload->data("file_name");

				$this->edit_produk($produk);
			}
		}
	}

	public function addPenjualan () {
		$data = $this->input->post();

		$this->load->database();

		if ($data['tanggal_waktu'] && $data['total_harga'] && $data['penjualanDetail']) {
			$ins = array(
				'tanggal_waktu' => $data['tanggal_waktu'],
				'catatan' => $data['catatan'],
				'total_harga' => $data['total_harga']
			);	
		
			$login_status = $this->db->insert('tbl_penjualan', $ins);
			$id_penjualan = $this->db->insert_id();

			foreach ($data['penjualanDetail'] as $key => $value) {
				$ins = array(
					'id_penjualan' => $id_penjualan,
					'id_produk' => $data['penjualanDetail'][$key]['id_produk'],
					'harga_satuan' => $data['penjualanDetail'][$key]['harga_satuan'],
					'jumlah' => $data['penjualanDetail'][$key]['jumlah']
				);

				$this->db->insert('tbl_penjualan_detail', $ins);

				$stok = $this->db
							->select('stok')
							->from('tbl_produk')
							->where('id', $data['penjualanDetail'][$key]['id_produk'])
							->get()
							->row_array();

				$stok = $stok['stok'];

				$new_stok = floatval($stok) - floatval($data['penjualanDetail'][$key]['jumlah']);

				$ups = array(
					'stok' => $new_stok
				);

				$this->db->where('id', $data['penjualanDetail'][$key]['id_produk']);
				$this->db->update('tbl_produk', $ups);
			}
		} else {
			$login_status = false;
		}

		echo json_encode(array(
			'status' => $login_status
		)); die;
	}

	public function hapusPenjualan () {
		$data = $this->input->post();

		$this->load->database();

		if ($data['id_penjualan']) {
			$detail = $this->db
						->select('*')
						->from('tbl_penjualan_detail')
						->where('id_penjualan', $data['id_penjualan'])
						->get()
						->result_array();

			foreach ($detail as $key => $value) {
				$stok = $this->db
							->select('stok')
							->from('tbl_produk')
							->where('id', $detail[$key]['id_produk'])
							->get()
							->row_array();

				$stok = $stok['stok'];

				$new_stok = floatval($stok) + floatval($detail[$key]['jumlah']);

				$ups = array(
					'stok' => $new_stok
				);

				$this->db->where('id', $detail[$key]['id_produk']);
				$this->db->update('tbl_produk', $ups);
			}

			$hapus_penjualan_status = $this->db->delete('tbl_penjualan', array('id' => $data['id_penjualan']));
			$hapus_penjualan_status = $this->db->delete('tbl_penjualan_detail', array('id_penjualan' => $data['id_penjualan']));
		} else {
			$hapus_penjualan_status = false;
		}

		echo json_encode(array(
			'status' => $hapus_penjualan_status
		)); die;
	}

	public function getPenjualanDetail () {
		$data = $this->input->post();

		$this->load->database();

		$response = array();

		if ($data['id_penjualan']) {
			$penjualan = $this->db
			->select('*')
			->from('tbl_penjualan')
			->where('id', $data['id_penjualan'])
			->get()
			->row_array();

			$penjualan['detail'] = $this->db
			->select('tbl_penjualan_detail.*, tbl_produk.nama, tbl_produk.img')
			->from('tbl_penjualan_detail')
			->join('tbl_produk', 'tbl_produk.id = tbl_penjualan_detail.id_produk')
			->where('id_penjualan', $data['id_penjualan'])
			->get()
			->result_array();

			$response['status'] = true;
			$response['penjualan'] = $penjualan;
		} else {
			$response['status'] = false;
		}

		echo json_encode($response); die;
	}

	public function editPenjualan () {
		$data = $this->input->post();

		$this->load->database();

		if ($data['id'] && $data['tanggal_waktu'] && $data['total_harga'] && $data['penjualanDetail']) {
			$ins = array(
				'tanggal_waktu' => $data['tanggal_waktu'],
				'catatan' => $data['catatan'],
				'total_harga' => $data['total_harga']
			);
		
			$this->db->where('id', $data['id']);
			$edit_status = $this->db->update('tbl_penjualan', $ins);

			$perubahan_stok = array();

			$detail = $this->db
						->select('*')
						->from('tbl_penjualan_detail')
						->where('id_penjualan', $data['id'])
						->get()
						->result_array();

			$this->db->delete('tbl_penjualan_detail', array('id_penjualan' => $data['id']));

			foreach ($detail as $key => $value) {
				$jumlah_before = array(
					'id_produk' => $detail[$key]['id_produk'],
					'jumlah' => floatval("-" . $detail[$key]['jumlah'])
				);

				array_push($perubahan_stok, $jumlah_before);
			}

			foreach ($data['penjualanDetail'] as $key => $value) {
				$kesamaan = false;

				foreach ($perubahan_stok as $kunci => $value) {
					if ($perubahan_stok[$kunci]['id_produk'] == $data['penjualanDetail'][$key]['id_produk']) {
						$perubahan_stok[$kunci]['jumlah'] = floatval($data['penjualanDetail'][$key]['jumlah']) + floatval($perubahan_stok[$kunci]['jumlah']);

						$kesamaan = true;
					}
				}

				if (!$kesamaan) {
					$jumlah_after = array(
						'id_produk' => $data['penjualanDetail'][$key]['id_produk'],
						'jumlah' => floatval($data['penjualanDetail'][$key]['jumlah'])
					);
	
					array_push($perubahan_stok, $jumlah_after);
				}

				$ins = array(
					'id_penjualan' => $data['id'],
					'id_produk' => $data['penjualanDetail'][$key]['id_produk'],
					'harga_satuan' => $data['penjualanDetail'][$key]['harga_satuan'],
					'jumlah' => $data['penjualanDetail'][$key]['jumlah']
				);

				$this->db->insert('tbl_penjualan_detail', $ins);
			}

			foreach ($perubahan_stok as $key => $value) {
				$stok = $this->db
							->select('stok')
							->from('tbl_produk')
							->where('id', $perubahan_stok[$key]['id_produk'])
							->get()
							->row_array();

				$stok = $stok['stok'];

				$new_stok = floatval($stok) - floatval($perubahan_stok[$key]['jumlah']);

				$ups = array(
					'stok' => $new_stok
				);

				$this->db->where('id', $perubahan_stok[$key]['id_produk']);
				$this->db->update('tbl_produk', $ups);
			}
		} else {
			$edit_status = false;
		}

		echo json_encode(array(
			'status' => $edit_status
		)); die;
	}

	public function getProdukbyId () {
		$data = $this->input->post();

		$this->load->database();

		$produk = $this->db
					->select('*')
					->from('tbl_produk')
					->where('id', $data['id'])
					->get()
					->row_array();

		echo json_encode($produk);
	}

	public function injeksiStok () {
		$data = $this->input->post();

		$this->load->database();

		$login_status = false;

		if ($data['tipe'] == 'tambah') {
			$stok = $data['stok'] + $data['injeksi'];
		}

		if ($data['tipe'] == 'kurang') {
			$stok = $data['stok'] - $data['injeksi'];
		}

		$ins = array(
			'stok' => $stok
		);
	
		$this->db->where('id', $data['id_produk']);
		$login_status = $this->db->update('tbl_produk', $ins);

		echo json_encode(array(
			'status' => $login_status
		)); die;
	}
}

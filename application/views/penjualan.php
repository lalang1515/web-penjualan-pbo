<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
<style>
    #tableSelectProduct td, #tableSelectProduct th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #tableSelectProduct th {
        padding-top: 12px;
        padding-bottom: 12px;
        background-color: #0e958f;
        color: white;
    }

    #tableSelectProduct tr:nth-child(even){background-color: #f2f2f2;}

    #tableSelectProduct tr:hover {background-color: #ddd;}

    #selectButtonGroup:hover button{
        background-color: #e6e6e6;
        border-color: #adadad;
    }
</style>
<div>
    <div style="text-align: left; padding-top: 1px; margin-bottom: 15px;">
        <h2>Penjualan</h2>
        <a style="display: none;" href="#" class="btn btn-md btn-primary" data-toggle="modal" data-target="#basicModal">ADD</a>
        <button onclick="modal_add();" class="btn btn-md btn-primary">ADD</button>
    </div>
    <table id="table_produk" class="display">
        <thead>
            <tr>
                <th>Tanggal & waktu penjualan</th>
                <th>Total harga</th>
                <th>Catatan</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($penjualan as $key => $value) { ?>
                <tr>\
                    <td><?php echo $penjualan[$key]['tanggal_waktu'] ?></td>
                    <td>Rp. <?php echo $penjualan[$key]['total_harga'] ?></td>
                    <td><?php echo $penjualan[$key]['catatan'] ?></td>
                    <td>
                        <button onclick='viewPenjualan(<?php echo $penjualan[$key]["id"] ?>);' class="btn btn-sm btn-info"><i class="fa-solid fa-eye"></i></button>
                        <button onclick='hapusPenjualan(<?php echo $penjualan[$key]["id"] ?>);' class="btn btn-sm btn-danger"><i class="fa-solid fa-trash"></i></button>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Tambah Penjualan</h4>
        </div>
        <div class="modal-body">
            <div style="text-align: left;" class="form-group">
                <label>Tanggal<span style="color: red;">*</span></label>
                <input id="tanggal_penjualan" name="tanggal_penjualan" class="form-control" type="datetime-local">
            </div>
            <div style="text-align: left;" class="form-group">
                <label>Catatan</label>
                <input id="catatan_penjualan" name="catatan_penjualan" class="form-control" placeholder="Catatan" type="text">
            </div>
            <div style="text-align: left;" class="form-group">
                <label>Daftar Produk</label>
                <div style="height: 18vh; overflow: scroll; margin-bottom: 10px;">
                    <table style="width: 100%;">
                        <tr>
                            <th style="width: 40%; text-align: center;">Produk</th>
                            <th style="width: 25%; text-align: center;">Harga satuan</th>
                            <th style="width: 25%; text-align: center;">Jumlah</th>
                            <th style="text-align: center;">Aksi</th>
                        </tr>
                        <tr>
                            <td id="zeroAddedProdukCol" style="width: 100%;" colspan="4"><button disabled type="button" class="btn btn-default" style="width: 100%; border-radius: 4px; cursor: default;">Belum ada produk yang ditambahkan</button></td>
                        </tr>
                        <tbody id="addedProdukTable">
                        </tbody>
                    </table>
                </div>
                <table style="width: 100%;">
                    <tr>
                        <td style="padding-bottom: 10px;" colspan="2"></td>
                        <td style="padding-bottom: 10px;">Total harga:</td>
                        <td style="padding-bottom: 10px;">Rp. <span id="totalHargaKabeh">0</span></td>
                    </tr>
                    <tr>
                        <td style="width: 5%;"><button onclick="addProductToList();" type="button" class="btn btn-default" style="width: 100%; border-radius: 4px 0 0 4px;">+</button></td>
                        <td style="width: 35%;">
                            <input style="display: none;" id="idProdukSelected">
                            <div id="selectButtonGroup" style="width: 100%;" class="btn-group btn-group-justified" role="group" onclick="pick_product();">
                                <button id="buttonPickProduct" type="button" class="btn btn-default" style="width: 85.4%; border-radius: 0;">
                                    Pilih produk
                                </button>
                                <button style="border-radius: 0; width: 15%;" type="button" class="btn btn-default">
                                    <i class="fa-solid fa-caret-down"></i>
                                </button>
                            </div>
                        </td>
                        <td style="width: 20%;"><input id="inputHargaSatuan" class="form-control" style="width: 100%; border-radius: 0; cursor: default; text-align: center;" placeholder="Harga satuan" disabled></input></td>
                        <td style="width: 20%;"><input id="inputStok" class="form-control" style="width: 100%; border-radius: 0; cursor: default; text-align: center;" placeholder="Stok tersedia" disabled></input></td>
                        <td style="width: 20%;"><input id="jumlahProduk" onclick="limitInputJumlahAdd();" type="number" class="form-control" style="width: 100%; border-radius: 0 4px 4px 0; text-align: center;" placeholder="Jumlah"></input></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button id="submitAddEditButton" onclick="addPenjualan();" type="button" class="btn btn-primary">Submit</button>
        </div>
        </div>
    </div>
</div>
<div class="modal fade" id="pickProduct" tabindex="-1" role="dialog" aria-labelledby="pickProduct">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa-solid fa-magnifying-glass"></i></span>
                    <input type="text" class="form-control" id="searchProduct" placeholder="Ketik nama produk">
                </div>
                <div style="margin-top: 5px; width: 100%; height: 50vh; overflow: scroll;">
                    <table class="table" id="tableSelectProduct" style="width: 100%; border-collapse: collapse;">
                            <tr>
                                <td colspan="4">Ketik nama produk pada pencarian</td>
                            </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="close_pick_product();">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" style="z-index: 1000;" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModal">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" onclick="closeViewPenjualan();" class="close" aria-hidden="true">&times;</button>
            <h4 class="modal-title">View Penjualan</h4>
        </div>
        <div class="modal-body">
            <div style="text-align: left;" class="form-group">
                <label>Tanggal<span style="color: red;">*</span></label>
                <input id="tanggal_penjualan_view" disabled name="tanggal_penjualan_view" class="form-control" type="datetime-local">
            </div>
            <div style="text-align: left;" class="form-group">
                <label>Catatan</label>
                <input id="catatan_penjualan_view" disabled name="catatan_penjualan_view" class="form-control" placeholder="Catatan" type="text">
            </div>
            <div style="text-align: left;" class="form-group">
                <label>Daftar Produk</label>
                <div style="height: 25vh; overflow: scroll; margin-bottom: 10px;">
                    <table style="width: 100%;">
                        <tr>
                            <th style="width: 30%; text-align: center;">Gambar</th>
                            <th style="width: 25%; text-align: center;">Produk</th>
                            <th style="width: 20%; text-align: center;">Harga satuan</th>
                            <th style="width: 15%; text-align: center;">Jumlah</th>
                        </tr>
                        <tr>
                            <td id="zeroAddedProdukColView" style="width: 100%;" colspan="4"><button disabled type="button" class="btn btn-default" style="width: 100%; border-radius: 4px; cursor: default;">Belum ada produk yang ditambahkan</button></td>
                        </tr>
                        <tbody id="addedProdukTableView">
                        </tbody>
                    </table>
                </div>
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;">Total harga: Rp. <span id="totalHargaKabehView">0</span></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" onclick="closeViewPenjualan();" class="btn btn-default">Close</button>
            <button id="viewEditBtn" onclick="editPenjualan();" type="button" class="btn btn-warning">Edit</button>
        </div>
        </div>
    </div>
</div>
<script>    
    $(document).ready( function () {
        $('#table_produk').DataTable();
    } );

    $("[data-toggle='modal']").on("click", function(e){
        $("#basicModal").addClass("in");
        $("#basicModal").css("display", "block");
    });

    $("[data-dismiss='modal']").on("click", function(e){
        $("#basicModal").removeClass("in");
        $("#basicModal").css("display", "none");

        jumlahProduk = 0;
        $("#tanggal_penjualan").val('');
        $("#catatan_penjualan").val('');
        $("#totalHargaKabeh").html(0);
        $("#addedProdukTable").html('');

        $("#myModalLabel").html('Tambah Penjualan');
        $('#submitAddEditButton').attr('onclick', 'addPenjualan();');
    });

    function pick_product () {
        $("#pickProduct").addClass("in");
        $("#pickProduct").css("display", "block");
    }

    function close_pick_product () {
        $("#searchProduct").val('');
        $('#tableSelectProduct').html(`<tr>
            <td colspan="4">Ketik nama produk pada pencarian</td>
        </tr>`);

        $("#pickProduct").removeClass("in");
        $("#pickProduct").css("display", "none");
    }

    $("#searchProduct").keyup(function() {
        let textToSearch = $(this).val();

        if (textToSearch.length > 0) {
            $.post("http://localhost/web-penjualan-pbo/index.php/welcome/get_produk", {textToSearch: textToSearch}, function(data, status){
                data = JSON.parse(data);

                let row = '';

                row += `<tr>
                    <th style="width: 25%; text-align: center;">Gambar</th>
                    <th style="width: 25%; text-align: center;">Nama produk</th>
                    <th style="width: 25%; text-align: center;">Harga satuan</th>
                    <th style="width: 25%; text-align: center;">Aksi</th>
                </tr>`;
                
                if (data.length > 0) {
                    data.map(item => {
                        row += `<tr>
                        <td style="width: 25%; text-align: center;"><img alt="Gambar `+ item.nama +`" height="100px" width="100px" src="http://localhost/web-penjualan-pbo/application/uploads/produk/`+ item.img +`"></td>
                        <td style="width: 25%; text-align: center;">`+ item.nama +`</td>
                        <td style="width: 25%; text-align: center;">`+ item.harga_satuan +`</td>
                        <td style="width: 25%; text-align: center;"><button type="button" onclick='selectedItem(`+ JSON.stringify(item)+`);' class="btn btn-primary">Pilih</button></td>
                        </tr>`;
                    })
                } else {
                    row += `<tr>
                        <td colspan="4">Tidak ada produk dengan nama tersebut</td>
                        </tr>`;
                }

                $('#tableSelectProduct').html(row);
            });
        } else {
            $('#tableSelectProduct').html(`<tr>
                <td colspan="4">Ketik nama produk pada pencarian</td>
            </tr>`);
        }

    });

    function selectedItem (product) {
        close_pick_product();

        $("#buttonPickProduct").html(product.nama);
        $("#inputHargaSatuan").val(product.harga_satuan);
        $("#inputStok").val(product.stok);
        $("#idProdukSelected").val(product.id);

        limitInputJumlahAdd();

        console.log(product);
    }

    function modal_add () {
        $("[data-toggle='modal']").click();
    }

    let jumlahProduk = 0;

    function addProductToList () {
        let produk_id = $("#idProdukSelected").val();
        let nama_produk = $("#buttonPickProduct").html();
        let harga_satuan = $("#inputHargaSatuan").val();
        let jumlah = $("#jumlahProduk").val();
        let stok = $("#inputStok").val();

        if (produk_id == '') {
            alert("Kamu belum memilih produk!");

            return false;
        }
        if (jumlah == '' || jumlah == 0) {
            alert("Kamu belum mengisi jumlah produk!");

            return false;
        }
        if (parseFloat(jumlah) > parseFloat(stok)) {
            alert("Stok tidak mencukupi");
            limitInputJumlahAdd();

            return false;
        }

        if (jumlahProduk != 0) {
            let checkIdProduk = [];

            $('#addedProdukTable > tr').each(function() {
                checkIdProduk.push($(this).find("#id_produk").val());
            });

            if (checkIdProduk.includes("" + produk_id)) {
                alert('Kamu sudah menambahkan produk ini');

                return false;
            }
        }

        jumlahProduk++;

        let row = `<tr>
                        <td style="width: 40%;">
                            <input id="id_produk" value="` + produk_id + `" style="display: none;">
                            <button disabled type="button" class="btn btn-default" style="width: 100%; border-radius: 0; cursor: default;">
                                ` + nama_produk + `
                            </button>
                        </td>
                        <td style="width: 25%;"><input class="form-control" style="width: 100%; border-radius: 0; cursor: default; text-align: center;" id="hargaSatuanAdded" value="`+ harga_satuan +`" placeholder="Harga satuan" disabled></input></td>
                        <td style="width: 25%;"><input style="display: none;" value="`+ stok +`" id="stokAdded"></input><input id="jumlahAdded" onkeyup="limitInputJumlah(this, value);" onchange="limitInputJumlah(this, value);" type="number" class="form-control" style="width: 100%; border-radius: 0; text-align: center;" value="`+ jumlah +`" placeholder="Jumlah"></input></td>
                        <td><button style="border-radius: 0 4px 4px 0;" onclick="removeProduk(this);" class="btn btn-danger">delete</button></td>
                    </tr>`;

        $("#addedProdukTable").html($("#addedProdukTable").html() + row);
        $("#zeroAddedProdukCol").hide();

        hitungTotalHarga();
    }

    function removeProduk (component) {
        $(component).parent().parent().remove();

        jumlahProduk--;

        if (jumlahProduk == 0) {
            $("#zeroAddedProdukCol").show();
        }

        hitungTotalHarga();
    }

    function hitungTotalHarga () {
        let totalHarga = 0;

        $('#addedProdukTable > tr').each(function() {
            let hargaSatuan = parseFloat($(this).find("#hargaSatuanAdded").val());
            let jumlah = parseFloat($(this).find("#jumlahAdded").val());

            totalHarga += hargaSatuan * jumlah;
        });

        $("#totalHargaKabeh").html(totalHarga);
    }

    function limitInputJumlah (component, value) {
        let stok = $(component).parent().find("#stokAdded").val();

        if (parseFloat(value) < 1 || value == '') {
            $(component).val(1);
        }

        if (parseFloat(value) > parseFloat(stok)) {
            $(component).val(parseFloat(stok));

            alert('Stok tidak mencukupi!');
        }

        hitungTotalHarga();
    }

    function limitInputJumlahAdd () {
        let value = $("#jumlahProduk").val();

        if (parseFloat(value) < 1 || value == '') {
            $("#jumlahProduk").val(1);
        }
        if (parseFloat(value) > $("#inputStok").val()) {
            $("#jumlahProduk").val($("#inputStok").val());
        }

        hitungTotalHarga();
    }

    function addPenjualan (id = '') {
        let datetime = $("#tanggal_penjualan").val();

        if (datetime == '') {
            alert('Tanggal dan waktu harus diisi!');
            return false;
        }

        let notes = $("#catatan_penjualan").val();
        let total_harga = $("#totalHargaKabeh").html();

        let penjualanDetail = [];

        $('#addedProdukTable > tr').each(function() {
            let id_produk = parseFloat($(this).find("#id_produk").val());
            let hargaSatuan = parseFloat($(this).find("#hargaSatuanAdded").val());
            let jumlah = parseFloat($(this).find("#jumlahAdded").val());

            let produk = {
                id_produk: id_produk,
                harga_satuan: hargaSatuan,
                jumlah: jumlah
            };

            penjualanDetail.push(produk);
        });

        if (penjualanDetail.length == 0) {
            alert('Harus ada produk yang ditambahkan!');
            return false;
        }

        let data = {
            tanggal_waktu: datetime,
            catatan: notes,
            total_harga: total_harga,
            penjualanDetail: penjualanDetail
        };

        if (id == '') {
            $.post("http://localhost/web-penjualan-pbo/index.php/welcome/addPenjualan", data, function(data, status){
                data = JSON.parse(data);

                if(data.status) {
                    alert('Tambah penjualan berhasil!');
                    window.location.href = "http://localhost/web-penjualan-pbo/index.php/welcome/beranda/penjualan";
                } else {
                    alert('Tambah penjualan gagal!');
                }
            });
        } else {
            data = {...data,
                id: id
            };

            $.post("http://localhost/web-penjualan-pbo/index.php/welcome/editPenjualan", data, function(data, status){
                data = JSON.parse(data);

                if(data.status) {
                    alert('Edit penjualan berhasil!');
                    window.location.href = "http://localhost/web-penjualan-pbo/index.php/welcome/beranda/penjualan";
                } else {
                    alert('Edit penjualan gagal!');
                }
            });
        }
    }

    function hapusPenjualan (id_penjualan) {
        let hapus_confirm = confirm('Yang bener mau hapus?');

        if (hapus_confirm) {
            let data = {
                id_penjualan: id_penjualan
            };

            $.post("http://localhost/web-penjualan-pbo/index.php/welcome/hapusPenjualan", data, function(data, status){
                data = JSON.parse(data);

                if(data.status) {
                    alert('Hapus penjualan berhasil!');
                    window.location.href = "http://localhost/web-penjualan-pbo/index.php/welcome/beranda/penjualan";
                } else {
                    alert('Hapus penjualan gagal!');
                }
            });
        }
    }

    function viewPenjualan (id_penjualan) {
        let data = {
            id_penjualan: id_penjualan
        };

        $.post("http://localhost/web-penjualan-pbo/index.php/welcome/getPenjualanDetail", data, function(data, status){
            data = JSON.parse(data);

            if (data.status) {
                console.log(data);

                let penjualan = data.penjualan;

                $('#viewEditBtn').attr('onclick', `editPenjualan(`+ JSON.stringify(penjualan) +`);`);

                penjualan.tanggal_waktu = penjualan.tanggal_waktu.replace(' ', 'T');

                $("#tanggal_penjualan_view").val(penjualan.tanggal_waktu);
                $("#catatan_penjualan_view").val(penjualan.catatan);
                $("#totalHargaKabehView").html(penjualan.total_harga);

                let row = ``;

                penjualan.detail.map(item => {
                    row += `<tr>
                        <td style="width: 30%; text-align: center;">
                            <img alt="Gambar ` + item.nama + `" height="100px" width="100px" src="http://localhost/web-penjualan-pbo/application/uploads/produk/` + item.img + `">
                        </td>
                        <td style="width: 25%;">
                            <input id="id_produk" value="` + item.id_produk + `" style="display: none;">
                            <button disabled type="button" class="btn btn-default" style="width: 100%; border-radius: 0; cursor: default;">
                                ` + item.nama + `
                            </button>
                        </td>
                        <td style="width: 20%;"><input class="form-control" style="width: 100%; border-radius: 0; cursor: default; text-align: center;" id="hargaSatuanAdded" value="`+ item.harga_satuan +`" placeholder="Harga satuan" disabled></input></td>
                        <td style="width: 15%;"><input id="jumlahAdded" disabled type="number" class="form-control" style="width: 100%; border-radius: 0; text-align: center;" value="`+ item.jumlah +`" placeholder="Jumlah"></input></td>
                    </tr>`;
                })

                $("#addedProdukTableView").html(row);
                $("#zeroAddedProdukColView").hide();
            } else {
                alert('ID penjualan tidak ada!');
            }
        });

        $("#viewModal").addClass("in");
        $("#viewModal").css("display", "block");
    }

    function closeViewPenjualan () {
        $("#viewModal").removeClass("in");
        $("#viewModal").css("display", "none");
    }

    function editPenjualan (data) {
        $("[data-toggle='modal']").click();
        $("#myModalLabel").html('Edit Penjualan');

        data.tanggal_waktu = data.tanggal_waktu.replace(' ', 'T');
        $("#tanggal_penjualan").val(data.tanggal_waktu);
        $("#catatan_penjualan").val(data.catatan);

        let jumlahProdukToBeEdited = 0;

        let row = ``;

        data.detail.map(item => {
            row += `<tr>
                        <td style="width: 40%;">
                            <input id="id_produk" value="` + item.id_produk + `" style="display: none;">
                            <button disabled type="button" class="btn btn-default" style="width: 100%; border-radius: 0; cursor: default;">
                                ` + item.nama + `
                            </button>
                        </td>
                        <td style="width: 25%;"><input class="form-control" style="width: 100%; border-radius: 0; cursor: default; text-align: center;" id="hargaSatuanAdded" value="`+ item.harga_satuan +`" placeholder="Harga satuan" disabled></input></td>
                        <td style="width: 25%;"><input style="display: none;" value="`+ item.stok +`" id="stokAdded"></input><input id="jumlahAdded" onkeyup="limitInputJumlah(this, value);" onchange="limitInputJumlah(this, value);" type="number" class="form-control" style="width: 100%; border-radius: 0; text-align: center;" value="`+ item.jumlah +`" placeholder="Jumlah"></input></td>
                        <td><button style="border-radius: 0 4px 4px 0;" onclick="removeProduk(this);" class="btn btn-danger">delete</button></td>
                    </tr>`;
            
            jumlahProdukToBeEdited++;
        })

        if (jumlahProdukToBeEdited > 0) {
            $("#zeroAddedProdukCol").hide();
        }

        $("#addedProdukTable").html(row);

        hitungTotalHarga();

        $('#submitAddEditButton').attr('onclick', 'addPenjualan('+ data.id +');');

        jumlahProduk = jumlahProdukToBeEdited;
    }

</script>
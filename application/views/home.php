<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Jolali</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <style>
        @media (min-width: 992px) {
            #responsive_border1 {
                border-radius: 5px 0 0 5px;
            }
            #responsive_border2 {
                border-radius: 0 5px 5px 0;
            }
        }
        @media (max-width: 992px) {
            #responsive_border1 {
                border-radius: 5px 5px 0 0;
            }
            #responsive_border2 {
                border-radius: 0 0 5px 5px;
            }
        }
    </style>
</head>
<body>
<div style="background-color: #cccccc; height: 100vh; position: relative;">
    <div class="container" style="position: absolute; top: 50%; transform: translateY(-50%) translateX(-50%); left: 50%; padding: 0;">
        <div style="box-shadow: 1px 5px 10px rgb(0 0 0 / 0.2);">
            <div style="background-image: url(https://img.freepik.com/free-vector/nature-scene-with-river-hills-forest-mountain-landscape-flat-cartoon-style-illustration_1150-37326.jpg?w=2000); height: 400px; text-align: center;" id="responsive_border1" class="col-md-6">
                <h2 style="color: white; position: relative; top: 40%;">Jolali Sales</h2>
            </div>
            <div id="responsive_border2" class="col-md-6" style="height: 400px; background-color: white; text-align: center; position: relative;">
                <div style="width: 70%; position: absolute; top: 50%; transform: translateY(-50%) translateX(-50%); left: 50%;">
                    <h2 style="color: #109891;">Login</h2>

                    <div class="panel">
                        <div class="panel-body">
                            <div class="form-group">
                                <input id="email" class="form-control" required placeholder="Email" type="email">
                            </div>
                            <div class="form-group">
                                <input id="password" class="form-control" required placeholder="Password" type="password">
                            </div>

                            <button class="btn btn-primary" style="background-color: #109891; width: 100%;" onclick="login()">Login</button>
                            <h5>Don't have an account? <a href="http://localhost/web-penjualan-pbo/index.php/welcome/registery">Sign Up</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function login () {
        let email = $('#email').val();
        let password = $('#password').val();

        if (email && password) {
            $.post("http://localhost/web-penjualan-pbo/index.php/welcome/login", [], function(data, status){
                data = JSON.parse(data);

                let login = false;

                data.map(item => {
                    if (item.email == email && item.password == password) {
                        login = true;
                    }
                })

                if (login) {
                    alert('Login berhasil!');
                    window.location.href = "http://localhost/web-penjualan-pbo/index.php/welcome/beranda/produk";
                } else {
                    alert('Login gagal!');
                }
            });
        } else {
            alert('Login gagal!');
        }
    }

	function welcome () {
		window.location.href = "http://localhost/web-penjualan-pbo";
	}
</script>
</html>

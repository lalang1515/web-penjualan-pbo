<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Jolali</title>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://kit.fontawesome.com/1eca5621d3.js" crossorigin="anonymous"></script>

    <style>
        @media (min-width: 992px) {
            #responsive_border1 {
                border-radius: 5px 0 0 5px;
                height: 90vh;
            }
            #responsive_border2 {
                border-radius: 0 5px 5px 0;
            }
            #navbar_links {
                text-decoration: none;
            }
            #navbar_wrap {
                height: 80vh; 
            }
            #navbar_wrap2 {
                height: 70px; 
            }
        }
        @media (max-width: 992px) {
            #responsive_border1 {
                border-radius: 5px 5px 0 0;
                height: 40px;
            }
            #responsive_border2 {
                border-radius: 0 0 5px 5px;
            }
            #navbar_links {
                padding: 4px 10px;
                float: left; 
                margin-right: 5px;
                text-decoration: none;
            }
            #navbar_wrap {
                display: inline;
            }
            #navbar_wrap2 {
                height: 40px;
            }
        }
        .sidebar {
            margin-top: 15px;
        }
        .sidebar a {
            display: block;
            margin-top: 5px;
            margin-bottom: 5px;
            padding-top: 5px;
            text-decoration: none;
            background-color: #4bc2a3;
            color: white;
            cursor: pointer;
            height: 30px;
            border-radius: 2px;
        }
        .sidebar a:hover {
            background-color: #8ef3ba;
            color: #4bc2a3;
        }
        #selected {
            background-color: #8ef3ba;
            color: #4bc2a3;
        }
    </style>
</head>
<body>
<div style="display: flex; background-color: #cccccc; min-height: 100vh; position: relative; ">
    <div class="container" style="position: absolute; top: 50%; transform: translateY(-50%) translateX(-50%); left: 50%; padding: 0;">
        <div style="box-shadow: 1px 5px 10px rgb(0 0 0 / 0.2);">
            <div style="background-image: url(https://img.freepik.com/free-vector/nature-scene-with-river-hills-forest-mountain-landscape-flat-cartoon-style-illustration_1150-37326.jpg?w=2000); text-align: center;" id="responsive_border1" class="col-md-2">
                <div class="sidebar">
                    <!-- <a href="http://localhost/web-penjualan-pbo/index.php/welcome/beranda">Beranda</a> -->
                    <div id="navbar_wrap">
                        <a id="navbar_links" href="http://localhost/web-penjualan-pbo/index.php/welcome/beranda/produk">Produk</a>
                        <a id="navbar_links" href="http://localhost/web-penjualan-pbo/index.php/welcome/beranda/penjualan">Penjualan</a>
                    </div>
                    <div id="navbar_wrap2">
                        <a id="navbar_links" onclick="logout();"><i class="fa-solid fa-power-off"></i></a>
                    </div>
                </div>
            </div>
            <div id="responsive_border2" class="col-md-10" style="height: 90vh; background-color: white; text-align: center; position: relative;">
                <?php $this->load->view($path) ?>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    function logout() {
        let yes = confirm('Do you want to log out?');
        if (yes) {
            window.location.href = 'http://localhost/web-penjualan-pbo/index.php/welcome/home';
        }
    }
</script>
</html>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
<style>
    .tipeKelola label {
        padding: 20px;
    }

    #optionTambah:checked + #labelTambah {
        background-color: green;
        color: white;
    }

    #optionKurang:checked + #labelKurang {
        background-color: green;
        color: white;
    }

    [type=radio] {
    margin:0;padding:0;
    -webkit-appearance:none;
        -moz-appearance:none;
            appearance:none;
    }
</style>
<div>
    <div style="text-align: left; padding-top: 1px; margin-bottom: 15px;">
        <h2>Produk</h2>
        <a style="display: none;" href="#" class="btn btn-md btn-primary" data-toggle="modal" data-target="#basicModal">ADD</a>
        <button onclick="modal_add();" class="btn btn-md btn-primary">ADD</button>
    </div>
    <div style="overflow: scroll; height: 65vh;">
        <table id="table_produk" class="display">
            <thead>
                <tr>
                    <th>Gambar</th>
                    <th>Nama produk</th>
                    <th>Harga satuan</th>
                    <th>Stok tersedia</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($produk as $key => $value) { ?>
                    <tr>
                        <td>
                            <img alt="Gambar <?php echo $produk[$key]['nama'] ?>" height="100px" width="100px" src="http://<?php echo base_url() ?>application/uploads/produk/<?php echo $produk[$key]['img'] ?>">
                        </td>
                        <td><?php echo $produk[$key]['nama'] ?></td>
                        <td>Rp. <?php echo $produk[$key]['harga_satuan'] ?></td>
                        <td><?php echo $produk[$key]['stok'] ?></td>
                        <td>
                            <button onclick='stock_modal(<?php echo $produk[$key]['id'] ?>);' class="btn btn-sm btn-success"><i class="fa-solid fa-boxes-stacked"></i></button>
                            <button onclick='edit_modal(<?php echo json_encode($produk[$key]) ?>);' class="btn btn-sm btn-warning"><i class="fa-solid fa-pen-to-square"></i></button>
                            <button onclick='hapus(<?php echo json_encode($produk[$key]) ?>);' class="btn btn-sm btn-danger"><i class="fa-solid fa-trash"></i></button>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Tambah Produk</h4>
        </div>
        <?php echo form_open_multipart('http://localhost/web-penjualan-pbo/index.php/welcome/do_upload');?>
            <div class="modal-body">
                <div style="text-align: left;" class="form-group">
                    <label>Nama produk</label>
                    <input id="nama_produk" name="nama_produk" class="form-control" required placeholder="Nama produk" type="text">
                </div>
                <div style="text-align: left;" class="form-group">
                    <label>Harga satuan</label>
                    <input id="harga_satuan" name="harga_satuan" class="form-control" required placeholder="Harga satuan" type="number">
                </div>
                <div style="text-align: left;" class="form-group">
                    <input style="display: none;" id="existing_img" type="text" name="img">
                    <label>Pilih gambar</label>
                    <input required type="file" id="produk_img" class="form-control" name="produk_img" accept="image/png, image/jpg, image/jpeg"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="modalSubmit" type="submit" class="btn btn-primary">Tambah</button>
                <!-- <input name="submit" type="submit" value="Submit"> -->
            </div>
        </form>
        </div>
    </div>
</div>
<div class="modal fade" id="stockControlModal" tabindex="-1" role="dialog" aria-labelledby="stockControlModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" onclick="closeStockModal();" class="close" aria-hidden="true">&times;</button>
                <h4 id="stockModalLabel" class="modal-title">Kelola Stok</h4>
            </div>
            <div class="modal-body">
                <div style="text-align: left;" class="form-group">
                    <label>Stok terkini</label>
                    <input id="stokTerkini" class="form-control" disabled placeholder="Jumlah stok terkini" type="number">
                </div>
                <div style="text-align: left;" class="form-group">
                        <label>Tipe kelola stok</label>
                        <div class="tipeKelola">
                            <input type="radio" name="tipeKelolaStok" id="optionTambah" value="tambah" checked/>
                            <label id="labelTambah" class="btn btn-default" for="optionTambah">
                                <i class="fa-solid fa-plus"></i><br>
                                Tambah
                            </label>
                            <input type="radio" name="tipeKelolaStok" id="optionKurang" value="kurang"/>
                            <label onclick="checkKurangLimit();" id="labelKurang" class="btn btn-default" for="optionKurang">
                                <i class="fa-solid fa-minus"></i><br>
                                Kurang
                            </label>
                        </div>
                </div>
                <div style="text-align: left;" class="form-group">
                        <label>Jumlah</label>
                        <input id="stokInject" onchange="checkLimitInject();" class="form-control" placeholder="Jumlah stok yang akan diinject" type="number">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="closeStockModal();">Close</button>
                <button id="submitStokButton" onclick="submitStokInjection();" type="button" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
<script>
    
    $(document).ready( function () {
        $('#table_produk').DataTable();
    } );

    $("[data-toggle='modal']").on("click", function(e){
        $("#basicModal").addClass("in");
        $("#basicModal").css("display", "block");
    });

    $("[data-dismiss='modal']").on("click", function(e){
        $("#basicModal").removeClass("in");
        $("#basicModal").css("display", "none");
        $('#nama_produk').prop('required',true);
        $('#harga_satuan').prop('required',true);
        $('#produk_img').prop('required',true);
        $("form").removeAttr('onsubmit');

        $('#nama_produk').val('');
        $('#harga_satuan').val('');
        $("form").attr('action', 'http://localhost/web-penjualan-pbo/index.php/welcome/do_upload');
    });

    function modal_add () {
        $('#myModalLabel').text('Tambah Produk');
        $('#modalSubmit').text('Tambah');
        $("#modalSubmit").attr("onclick","tambah()");

        $("[data-toggle='modal']").click();
    }

    function tambah () {
        let nama_produk = $('#nama_produk').val();
        let harga_satuan = $('#harga_satuan').val();
        var input = document.getElementById('produk_img');
        // console.log(input.files);
    }

    function hapus (barang) {
        let hapus_confirm = confirm('Yang bener mau hapus?');

        let data = barang;

        if (hapus_confirm) {
            $.post("http://localhost/web-penjualan-pbo/index.php/welcome/hapus_produk", data, function(data, status){
                data = JSON.parse(data);

                if(data.status) {
                    alert('Hapus produk berhasil!');
                    window.location.href = "http://localhost/web-penjualan-pbo/index.php/welcome/beranda/produk";
                } else {
                    alert('Hapus produk gagal!');
                }
            });
        }
    }

    function edit_modal (barang) {
        $('#myModalLabel').text('Edit Produk');
        $('#modalSubmit').text('Edit');
        $('#nama_produk').prop('required',false);
        $('#harga_satuan').prop('required',false);
        $('#produk_img').prop('required',false);

        $('#nama_produk').val(barang.nama);
        $('#harga_satuan').val(barang.harga_satuan);
        $('#existing_img').val(barang.img);

        $("[data-toggle='modal']").click();
        $("form").attr('action', 'http://localhost/web-penjualan-pbo/index.php/welcome/edit_upload/' + barang.id);
        $("form").attr('onsubmit', 'return validateEdit(this);');
    }

    function validateEdit (form) {
        var input = document.getElementById('produk_img');

        if (input.files.length == 0 && $('#nama_produk').val() == '' && $('#harga_satuan').val() == '') {
            alert('Kamu tidak mengubah apapun!');
            return false;
        }
    }

    function stock_modal (id_produk) {
        let data = {
            id: id_produk
        }

        $.post("http://localhost/web-penjualan-pbo/index.php/welcome/getProdukbyId", data, function(data, status){
            data = JSON.parse(data);

            $("#stokTerkini").val(data.stok);
            $("#stockModalLabel").html('Kelola stok ' + data.nama);
        });

        $("#submitStokButton").attr('onclick', 'submitStokInjection('+ id_produk +');');
        $("#stockControlModal").addClass("in");
        $("#stockControlModal").css("display", "block");
    }

    function closeStockModal () {
        $("#stockControlModal").removeClass("in");
        $("#stockControlModal").css("display", "none");
    }

    function checkLimitInject () {
        let stok = parseFloat($("#stokTerkini").val());
        let value = $('#stokInject').val();
        
        if ($("[name='tipeKelolaStok']:checked").val() == 'kurang') {
            if (stok < parseFloat(value)) {
                $('#stokInject').val(stok);
            }
        }
    }

    function checkKurangLimit () {
        let stok = parseFloat($("#stokTerkini").val());
        let value = $('#stokInject').val();
        
        if (stok < parseFloat(value)) {
            $('#stokInject').val(stok);
        }
    }

    function submitStokInjection (id_produk) {
        let stok = parseFloat($("#stokTerkini").val());
        let tipe = $("[name='tipeKelolaStok']:checked").val();
        let injeksi = $("#stokInject").val();

        if (injeksi == 0) {
            alert('Kamu belum menginput jumlah injeksi dengan benar!');
        }

        let data = {
            id_produk: id_produk,
            stok: stok,
            tipe: tipe,
            injeksi, injeksi
        }

        $.post("http://localhost/web-penjualan-pbo/index.php/welcome/injeksiStok", data, function(data, status){
            data = JSON.parse(data);

            if(data.status) {
                alert('Injeksi stok berhasil!');
                window.location.href = "http://localhost/web-penjualan-pbo/index.php/welcome/beranda/produk";
            } else {
                alert('Injeksi stok gagal!');
            }
        });
    }
</script>